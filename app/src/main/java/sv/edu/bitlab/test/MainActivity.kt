package sv.edu.bitlab.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    val color = arrayOf(
        R.color.gris,
        R.color.rojo,
        R.color.amarillo,
        R.color.verde
    )
    val colorText = arrayOf(
        "Apagado",
        "Rojo",
        "Amarillo",
        "Verde"
    )
    var container: View?=null
    var SBtn: Button?=null
    var textView: TextView?=null
    var start = false
    val handler = Handler()
    var currentLights = GRAY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        container = findViewById(R.id.container)
        SBtn = findViewById(R.id.startBtn)
        textView = findViewById(R.id.textView)
        SBtn!!.setOnClickListener {
            if(!start){
                startLights()
            } else {
                stopLights()
            }
        }
    }

    fun changeColor(colorIndex: Int) {
        when(colorIndex){
            GRAY -> Log.d("SEMAFORO", "GRIS")
            RED -> Log.d("SEMAFORO", "RED")
            YELLOW -> Log.d("SEMAFORO", "YELLOW")
            GREEN -> Log.d("SEMAFORO", "GREEN")
        }
        val resourceColor = color[colorIndex]
        container!!.setBackgroundColor(ContextCompat.getColor(this, resourceColor))
        //container!!.setBackgroundColor(resources.getColor(resourceColor))
    }

    fun changeText(colorIndex: Int) {
        val text = when(colorIndex){
            RED -> "Alto"
            YELLOW -> "Precaucion"
            GREEN -> "Avance"
            else -> "Apagado"
        }
        textView!!.text = text
    }

    private fun startLights(){
        start = true
        currentLights = RED
        changeLights(currentLights)
    }

    private fun changeLights(color: Int){
        if(start){
            SBtn!!.text = "Detener"
            changeText(color)
            changeColor(color)
            currentLights = if(currentLights < GREEN) currentLights +1 else RED
            handler.postDelayed({
                runOnUiThread {
                    changeLights(currentLights)
                }
            }, FIVE_SECONDS)
        }
    }

    private fun stopLights(){
        start = false
        SBtn!!.text = "Iniciar"
        changeColor(GRAY)
        changeText(GRAY)
    }

    companion object{
        const val GRAY = 0
        const val RED = 1
        const val YELLOW = 2
        const val GREEN = 3
        const val FIVE_SECONDS = 5000L
    }
}
